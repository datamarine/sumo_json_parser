﻿Function Get-FolderPath($initialDirectory)
{   
     [System.Reflection.Assembly]::LoadWithPartialName("System.windows.forms")|Out-Null

    $foldername = New-Object System.Windows.Forms.FolderBrowserDialog
    $foldername.Description = "Select a folder"
    $foldername.rootfolder = "UserProfile"

    if($foldername.ShowDialog() -eq "OK")
    {
        $folder = $foldername.SelectedPath
    }
    return $folder
}

While($files.count -le 0){

    $fpath = Get-FolderPath($env:userprofile)
    $files = Get-ChildItem $fpath | ?{$_.extension.tolower() -eq ".json"}
}

foreach($f in $files){
    $text = get-content $f.fullname | convertfrom-json
    if($text.name -eq $null){
        $oname = $f.name + ".csv"
        #$oname = $test.name + ".csv"
        $queries = $text.panels | select name,queryString
        $queries | export-csv "$fpath\$oname" -notypeinformation
    }
}    