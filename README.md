# sumo_json_parser

Parses exported Sumo dashboards to extract the panel name and query

## Getting started

Export the dashboards you want to work on converting from Sumologic in json format and place all of them into a folder. 

Run the sumo_json_parser.ps1 script. It will open a file browser window, browse to and select the folder containing your exported json files and hit ok. The script will parse all .json files in the folder and export the CSVs to the same location. 
